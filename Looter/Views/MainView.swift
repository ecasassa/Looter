//
//  MainView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/11/23.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            CategoryListView()
                .tabItem {
                    Label("Categories", systemImage: "list.dash")
                }
            EquipmentListView()
                .tabItem {
                    Label("All Equipment", systemImage: "list.dash")
                }
            RandomizerView()
                .tabItem {
                    Label("Loot!", systemImage: "shuffle")
                }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
