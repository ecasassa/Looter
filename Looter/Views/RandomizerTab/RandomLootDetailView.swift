//
//  RandomLootDetailView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/15/23.
//

import SwiftUI

struct RandomLootDetailView: View {
    @ObservedObject var slider = CustomSlider(start: 0, end: 10000)
    @StateObject var equipmentViewModel = EquipmentViewModel()
    
    @State private var randomEquipmentName = ""
    @State private var randomEquipmentIndexName = ""
    @State private var randomEquipmentURL = ""
    @State private var randomGP = 0
        
    var numberOfItemsSelected = 0
    var lowGPSelected = 0
    var highGPSelected = 10000
        
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    HStack {
                        Button {
                            randomGP = Int.random(in: Int(lowGPSelected)..<Int(highGPSelected))
                            print(randomGP)
                        } label: {
                            Text("Randomize GP")
                                .padding()
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color.white, lineWidth: 3)
                                )
                        }
                        
                        Text("\(randomGP)")
                            .bold()
                            .foregroundColor(.red)
                    }
                }
                
                Section {
                    HStack {
                        Button {
                            setRandomItem()
                            print(numberOfItemsSelected)
                            print(randomEquipmentName)
                        } label: {
                            Text("Randomize Item")
                                .padding()
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color.white, lineWidth: 3)
                                )
                        }
                    }
                    
                    NavigationLink() {
                        EquipmentDetailView(equipment: StandardCategory(indexName: "\(randomEquipmentIndexName)", name: "\(randomEquipmentName)", url: "\(randomEquipmentURL)"))
                    } label: {
                        Text("\(randomEquipmentName)")
                            .font(.title2)
                    }
                }
            }
            .task {
                await equipmentViewModel.fetchEquipment()
            }
        }
    }
    
    private func setRandomItem() {
        let randomItem = equipmentViewModel.equipmentArray.randomElement()
        randomEquipmentName = randomItem?.name ?? "couldn't get item"
        randomEquipmentIndexName = randomItem?.indexName ?? "couldn't get item"
        randomEquipmentURL = randomItem?.url ?? "/api/equipment/shortsword"
    }
}

struct RandomLootDetailView_Previews: PreviewProvider {
    static var previews: some View {
        RandomLootDetailView()
    }
}
