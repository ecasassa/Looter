//
//  RandomizerView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/11/23.
//

import SwiftUI

struct RandomizerView: View {
    
    @ObservedObject var slider = CustomSlider(start: 0, end: 10000)
    
    @State private var numberOfItems = 1
    @State private var randomGP = 0
    
    var body: some View {
        NavigationStack {
            VStack {
                HStack {
                    VStack {
                        let lowGP = slider.lowHandle.currentValue
                        let highGP = slider.highHandle.currentValue
                                                
                        Text("Gold Pieces Range")
                            .bold()
                                                
                        VStack() {
                            Text("Low Value: \(lowGP, specifier: "%.0f")")
                            Text("High Value: \(highGP, specifier: "%.0f")")
                            
                            SliderView(slider: slider)
                            
                        }
                    }
                }
                
                VStack {
                    // this does nothing for now
                    HStack {
                        Text("Items to Randomize")
                        
                        Picker("Number of items:", selection: $numberOfItems) {
                            ForEach(0..<21) {
                                Text("\($0)")
                            }
                        }
                    }
                }
                
                NavigationLink() {
                    RandomLootDetailView(numberOfItemsSelected: numberOfItems, lowGPSelected: Int(slider.lowHandle.currentValue), highGPSelected: Int(slider.highHandle.currentValue))
                } label: {
                    Text("Show me the money")
                        .font(.title2)
                }
                .disabled(slider.highHandle.currentValue<=slider.lowHandle.currentValue)
            }
            .navigationTitle("Looting Preferences")
        }
    }
}

struct RandomizerView_Previews: PreviewProvider {
    static var previews: some View {
        RandomizerView()
    }
}

    /*
     Inside NavigationStack:
        
     // This is the picker for nunmber of items
     
        Section {
            VStack {
                Text("Items to Randomize")
                
                Picker("Number of items:", selection: $numberOfItems) {
                    ForEach(1..<21) {
                        Text("\($0)")
                    }
                }
            }
        }
     
     // This randomizes one item
        
        Section {
            Button(action: setRandomItem) {
                Text("Randomize Item(s)")
                    .padding()
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.white, lineWidth: 3)
                    )
            }
            Text("\(randomEquipment)")
        }
    }
}
*/
