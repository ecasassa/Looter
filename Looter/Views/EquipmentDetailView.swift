//
//  EquipmentDetailView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import SwiftUI

struct EquipmentDetailView: View {
    @StateObject var equipmentDetailsViewModel = EquipmentDetailsViewModel()
    var equipment: StandardCategory
    
    var body: some View {
        VStack(alignment: .leading, spacing: 3) {
            Rectangle()
                .frame(height: 1)
                .foregroundColor(.gray)
                .padding(.bottom)
            
            HStack {
                VStack(alignment: .leading) {
                    HStack(alignment: .top) {
                        Text("Category:")
                            .font(.title2)
                            .bold()
                            .foregroundColor(.red)
                        
                        Text(equipmentDetailsViewModel.equipmentCategoryName ?? "")
                            .font(.title2)
                            .bold()
                            .lineLimit(nil)
                    }
                    
                    HStack(alignment: .top) {
                        Text("Cost:")
                            .font(.title2)
                            .bold()
                            .foregroundColor(.red)

                        Text(String(equipmentDetailsViewModel.quantity ?? 0) + String(equipmentDetailsViewModel.unit ?? ""))
                            .font(.title2)
                            .bold()
                    }
                    
                    HStack(alignment: .top) {
                        Text("Weight:")
                            .font(.title2)
                            .bold()
                            .foregroundColor(.red)
                        
                        Text(String(equipmentDetailsViewModel.weight ?? 0))
                            .font(.title2)
                            .bold()
                    }
                    
                    HStack(alignment: .top) {
                        Text("Description:")
                            .font(.title2)
                            .bold()
                            .foregroundColor(.red)
                    }
                }
            }
            
            VStack(alignment: .leading) {
                ForEach(descriptions, id: \.self) { description in
                    Text("\(description)")
                }
                .padding()
            }
            .padding()

            Spacer()
        }
        .navigationTitle(equipment.name)
        .padding()
        .task {
            equipmentDetailsViewModel.equipmentDetailsURLValue = "\(Constants.apiBaseURL)\(equipment.url)"
            await equipmentDetailsViewModel.fetchEquipmentDetails()
        }
    }
    
    private var descriptions: [String] {
        if equipmentDetailsViewModel.description == [] {
            return ["No description available"]
        } else {
            return equipmentDetailsViewModel.description ?? ["No description available"]
        }
    }
}

struct EquipmentDetailView_Previews: PreviewProvider {
    static var previews: some View {
        EquipmentDetailView(equipment: StandardCategory(indexName: "shortsword", name: "Shortsword", url: "\(Constants.apiBaseURL)/api/equipment/shortsword"))
    }
}
