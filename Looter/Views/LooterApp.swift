//
//  LooterApp.swift
//  Looter
//
//  Created by Etienne Casassa on 3/6/23.
//

import SwiftUI

@main
struct LooterApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
