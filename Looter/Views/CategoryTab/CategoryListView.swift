//
//  CategoryListView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/16/23.
//

import SwiftUI

struct CategoryListView: View {
    @StateObject var categoryViewModel = CategoryViewModel()
    @State private var searchText = ""
    
    var body: some View {
        NavigationStack {
            ZStack {
                List {
                    ForEach(searchResults) { category in
                        NavigationLink {
                            CategoryDetailView(categoryName: category)
                        } label: {
                            Text(category.name)
                                .font(.title2)
                        }
                    }
                }
                .listStyle(.plain)
                .navigationTitle("Equipment Categories")
                .toolbar {
                    ToolbarItem (placement: .status) {
                        Text("\(categoryViewModel.categoryArray.count) items")
                    }
                }
                .searchable(text: $searchText)
                
                if categoryViewModel.isLoading {
                    ProgressView()
                        .tint(.red)
                        .scaleEffect(4)
                }
            }
            .task {
                await categoryViewModel.fetchEquipment()
            }
        }
    }
    
    var searchResults: [StandardCategory] {
        if searchText.isEmpty {
            return categoryViewModel.categoryArray
        } else {
            return categoryViewModel.categoryArray.filter {$0.name.contains(searchText)}
        }
    }
}

struct CategoryListView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryListView()
    }
}
