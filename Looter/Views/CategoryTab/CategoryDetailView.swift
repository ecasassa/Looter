//
//  CategoryDetailView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/16/23.
//

import SwiftUI

struct CategoryDetailView: View {
    @StateObject var categoryDetailsViewModel = CategoryDetailsViewModel()
    @State private var searchText = ""
    
    var categoryName: StandardCategory
    
    var body: some View {
        NavigationStack {
            ZStack {
                List {
                    ForEach(searchResults) { equipment in
                        NavigationLink {
                            EquipmentDetailView(equipment: equipment)
                        } label: {
                            Text(equipment.name)
                                .font(.title2)
                        }
                    }
                }
                .listStyle(.plain)
                .navigationTitle("\(categoryName.name)")
                .toolbar {
                    ToolbarItem (placement: .status) {
                        Text("\(categoryDetailsViewModel.categoryEquipmentArray.count) items")
                    }
                }
                .searchable(text: $searchText)
                
                if categoryDetailsViewModel.isLoading {
                    ProgressView()
                        .tint(.red)
                        .scaleEffect(4)
                }
            }
            .task {
                categoryDetailsViewModel.categoryDetailsURLValue = "\(Constants.apiBaseURL)\(categoryName.url)"
                await categoryDetailsViewModel.fetchEquipment()
            }
        }
    }
    
    var searchResults: [StandardCategory] {
        if searchText.isEmpty {
            return categoryDetailsViewModel.categoryEquipmentArray
        } else {
            return categoryDetailsViewModel.categoryEquipmentArray.filter {$0.name.contains(searchText)}
        }
    }
}

struct CategoryDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryDetailView(categoryName: StandardCategory(indexName: "gaming-sets", name: "Gaming Sets", url: "\(Constants.apiBaseURL)/api/equipment-categories/gaming-sets"))
    }
}
