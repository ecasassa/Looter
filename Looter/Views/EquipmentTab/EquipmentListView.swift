//
//  EquipmentListView.swift
//  Looter
//
//  Created by Etienne Casassa on 3/6/23.
//

import SwiftUI

struct EquipmentListView: View {
    @StateObject var equipmentViewModel = EquipmentViewModel()
    @State private var searchText = ""
    
    var body: some View {
        NavigationStack {
            ZStack {
                List {
                    ForEach(searchResults) { equipment in
                        NavigationLink {
                            EquipmentDetailView(equipment: equipment)
                        } label: {
                            Text(equipment.name)
                                .font(.title2)
                        }
                    }
                }
                .listStyle(.plain)
                .navigationTitle("All Equipment")
                .toolbar {
                    ToolbarItem (placement: .status) {
                        Text("\(equipmentViewModel.equipmentArray.count) items")
                    }
                }
                .searchable(text: $searchText)
                
                if equipmentViewModel.isLoading {
                    ProgressView()
                        .tint(.red)
                        .scaleEffect(4)
                }
            }
            .task {
                await equipmentViewModel.fetchEquipment()
            }
        }
    }
    
    var searchResults: [StandardCategory] {
        if searchText.isEmpty {
            return equipmentViewModel.equipmentArray
        } else {
            return equipmentViewModel.equipmentArray.filter {$0.name.contains(searchText)}
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EquipmentListView()
    }
}
