//
//  CategoryDetailsModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/16/23.
//

import Foundation

struct CategoryDetailsModel: Codable, Identifiable {
    let id = UUID().uuidString

    var indexName: String
    var name: String
    var equipmentsArray: [StandardCategory]
    
    enum CodingKeys: String, CodingKey {
        case indexName = "index"
        case name
        case equipmentsArray = "equipment"
    }
}
