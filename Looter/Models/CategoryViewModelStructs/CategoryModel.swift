//
//  CategoryModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/16/23.
//

import Foundation

struct CategoryModel: Codable, Identifiable {
    let id = UUID().uuidString

    var count: Int
    var results: [StandardCategory]
    
    enum CodingKeys: String, CodingKey {
        case count
        case results
    }
}
