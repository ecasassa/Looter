//
//  StandardCategory.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

struct StandardCategory: Codable, Identifiable {
    let id = UUID().uuidString

    var indexName: String
    var name: String
    var url: String
    
    enum CodingKeys: String, CodingKey {
        case indexName = "index"
        case name
        case url
    }
}
