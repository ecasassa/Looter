//
//  EquipmentDetailsViewModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

@MainActor
class EquipmentDetailsViewModel: ObservableObject {
    var equipmentDetailsURLValue = ""

    @Published var name: String? = ""
    @Published var weight: Int? = 0

    @Published var description: [String]? = []
    @Published var quantity: Int? = 0
    @Published var unit: String? = ""
    @Published var equipmentCategoryName: String? = ""
    @Published var equipmentCategoryURL: String? = ""
    
    func fetchEquipmentDetails() async {
        guard let equipmentDetailsURL = URL(string: equipmentDetailsURLValue) else {
            print("^^^ Could not create a URL from: \(equipmentDetailsURLValue)")
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: equipmentDetailsURL)
            
            guard let equipmentDetailsReturned = try? JSONDecoder().decode(EquipmentResultsModel.self, from: data) else {
                print("^^^ JSON ERROR: Could not decode returned Details JSON Data for: \(equipmentDetailsURLValue)")
                return
            }
            self.name = equipmentDetailsReturned.name
            self.weight = equipmentDetailsReturned.weight

            self.description = equipmentDetailsReturned.description
            self.quantity = equipmentDetailsReturned.cost.quantity
            self.unit = equipmentDetailsReturned.cost.unit
            self.equipmentCategoryName = equipmentDetailsReturned.equipmentCategory.name
            self.equipmentCategoryURL = equipmentDetailsReturned.equipmentCategory.url
            
        } catch {
            print("^^^ Could not get data from \(equipmentDetailsURLValue). Error: \(error)")
        }
    }
}
