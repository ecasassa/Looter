//
//  CategoryDetailsViewModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/16/23.
//

import Foundation

@MainActor
class CategoryDetailsViewModel: ObservableObject {
    var categoryDetailsURLValue = ""

    @Published var categoryName = ""
    @Published var categoryEquipmentArray: [StandardCategory] = []
    @Published var isLoading = false
    
    func fetchEquipment() async {
        isLoading = true
        
        guard let equipmentURL = URL(string: categoryDetailsURLValue) else {
            print("^^^ Could not create a URL from: \(categoryDetailsURLValue)")
            isLoading = false
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: equipmentURL)
            
            guard let categoryDetailsReturned = try? JSONDecoder().decode(CategoryDetailsModel.self, from: data) else {
                print("^^^ JSON ERROR: Could not decode returned JSON Data")
                isLoading = false
                return
            }
            self.categoryName = categoryDetailsReturned.name
            self.categoryEquipmentArray = categoryDetailsReturned.equipmentsArray
            isLoading = false
        } catch {
            print("^^^ Could not get data from \(categoryDetailsURLValue). Error: \(error)")
            isLoading = false
        }
    }
}
