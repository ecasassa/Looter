//
//  EquipmentViewModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

@MainActor
class EquipmentViewModel: ObservableObject {
    @Published var equipmentURLValue = "\(Constants.apiBaseURL)\(Constants.equipmentURLsuffix)"
    @Published var count = 0
    @Published var equipmentArray: [StandardCategory] = []
    @Published var isLoading = false
    
    func fetchEquipment() async {
        isLoading = true
        
        guard let equipmentURL = URL(string: equipmentURLValue) else {
            print("^^^ Could not create a URL from: \(equipmentURLValue)")
            isLoading = false
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: equipmentURL)
            
            guard let equipmentReturned = try? JSONDecoder().decode(EquipmentModel.self, from: data) else {
                print("^^^ JSON ERROR: Could not decode returned JSON Data")
                isLoading = false
                return
            }
            self.count = equipmentReturned.count
            self.equipmentArray = equipmentReturned.results
            isLoading = false
        } catch {
            print("^^^ Could not get data from \(equipmentURLValue). Error: \(error)")
            isLoading = false
        }
    }
}
