//
//  CategoryViewModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/16/23.
//

import Foundation

@MainActor
class CategoryViewModel: ObservableObject {
    @Published var categoryURLValue = "\(Constants.apiBaseURL)\(Constants.equipmentCategoriesURLsuffix)"
    @Published var categoryCount = 0
    @Published var categoryArray: [StandardCategory] = []
    @Published var isLoading = false
    
    func fetchEquipment() async {
        isLoading = true
        
        guard let equipmentURL = URL(string: categoryURLValue) else {
            print("^^^ Could not create a URL from: \(categoryURLValue)")
            isLoading = false
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: equipmentURL)
            
            guard let categoriesReturned = try? JSONDecoder().decode(CategoryModel.self, from: data) else {
                print("^^^ JSON ERROR: Could not decode returned JSON Data")
                isLoading = false
                return
            }
            self.categoryCount = categoriesReturned.count
            self.categoryArray = categoriesReturned.results
            isLoading = false
        } catch {
            print("^^^ Could not get data from \(categoryURLValue). Error: \(error)")
            isLoading = false
        }
    }
}
