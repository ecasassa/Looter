//
//  QuantityUnit.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

struct QuantityUnit: Codable {
    var quantity: Int?
    var unit: String?
}
