//
//  ArmorClass.swift
//  Looter
//
//  Created by Etienne Casassa on 3/8/23.
//

import Foundation

struct ArmorClass: Codable {
    var base: Int
    var dexBonus: Bool
    var maxBonus: Int
    
    enum CodingKeys: String, CodingKey {
        case base
        case dexBonus = "dex_bonus"
        case maxBonus = "max_bonus"
    }
}

// maybe the solution to this might be something like: https://stackoverflow.com/questions/48297263/how-to-use-any-in-codable-type
// The documentation in https://www.dnd5eapi.co/docs/#get-/api/equipment/-index- says that armor_class might return [any-key]


/*
 
 Found a few examples that have armor_class
 
 https://www.dnd5eapi.co/api/equipment/breastplate
 https://www.dnd5eapi.co/api/equipment/chain-mail
 https://www.dnd5eapi.co/api/equipment/shield
 https://www.dnd5eapi.co/api/equipment/padded-armor
 https://www.dnd5eapi.co/api/equipment/hide-armor
 
 */
