//
//  Rarity.swift
//  Looter
//
//  Created by Etienne Casassa on 3/8/23.
//

import Foundation

struct Rarity: Codable {
    var rarity: String

    enum CodingKeys: String, CodingKey {
        case rarity = "name"
    }
}
