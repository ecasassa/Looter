//
//  Contents.swift
//  Looter
//
//  Created by Etienne Casassa on 3/8/23.
//

import Foundation

struct Contents: Codable {
    var item: StandardCategory
    var quantity: Int
}
