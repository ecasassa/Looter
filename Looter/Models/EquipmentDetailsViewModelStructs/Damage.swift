//
//  Damage.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

struct Damage: Codable {
    var damageDice: String
    var damageType: StandardCategory
    
    enum CodingKeys: String, CodingKey {
        case damageDice = "damage_dice"
        case damageType = "damage_type"
    }
}
