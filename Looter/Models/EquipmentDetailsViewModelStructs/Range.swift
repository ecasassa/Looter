//
//  Range.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

struct Range: Codable {
    var normal: Int
    var long: Int
}
