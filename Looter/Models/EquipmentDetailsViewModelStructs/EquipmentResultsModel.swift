//
//  EquipmentResultsModel.swift
//  Looter
//
//  Created by Etienne Casassa on 3/7/23.
//

import Foundation

struct EquipmentResultsModel: Codable, Identifiable {
    let id = UUID().uuidString
    
    var description: [String]?
//    var indexName: String
    var name: String?
    var equipmentCategory: StandardCategory

//    var equipment: [StandardCategory]
    var cost: QuantityUnit
    var weight: Int?
//    var properties: [StandardCategory]
    
//    var gearCategory: StandardCategory

//    var toolCategory: String

//    var vehicleCategory: String
//    var speed: QuantityUnit
//    var capacity: String

//    var armorCategory: String
//    var armorClass: ArmorClass
//    var strengthMinimum: Int
//    var stealthDisadvantage: Bool

//    var weaponCategory: String
//    var weaponRange: String
//    var categoryRange: String
//    var damage: Damage
//    var range: Range
//    var throwRange: Range
//    var twoHandedDamage: Damage

//    var rarity: Rarity
//    var variants: [StandardCategory]
//    var variant: Bool

//    var contents: [Contents]

    enum CodingKeys: String, CodingKey {
        case description = "desc"
//        case indexName = "index"
        case name
        case equipmentCategory = "equipment_category"

//        case equipment
        case cost
        case weight
//        case properties

//        case gearCategory = "gear_category"

//        case toolCategory = "tool_category"

//        case vehicleCategory = "vehicle_category"
//        case speed
//        case capacity

//        case armorCategory = "armor_category"
//        case armorClass = "armor_class"
//        case strengthMinimum = "str_minimum"
//        case stealthDisadvantage = "stealth_disadvantage"

//        case weaponCategory = "weapon_category"
//        case categoryRange = "category_range"
//        case weaponRange = "weapon_range"
//        case damage = "damage"
//        case range
//        case throwRange = "throw_range"
//        case twoHandedDamage = "two_handed_damage"

//        case rarity
//        case variants
//        case variant

//        case contents
    }
}
