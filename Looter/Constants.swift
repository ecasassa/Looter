//
//  Constants.swift
//  Looter
//
//  Created by Etienne Casassa on 3/6/23.
//

import Foundation

public class Constants {
    static let apiBaseURL = "https://www.dnd5eapi.co"
    static let equipmentURLsuffix = "/api/equipment/"
    static let equipmentCategoriesURLsuffix = "/api/equipment-categories/"
    static let magicItemsURLsuffix = "/api/magic-items/"
    static let weaponPropertiesURLsuffix = "/api/weapon-properties/"
}
