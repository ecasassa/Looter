//
//  ActionPlan.swift
//  Looter
//
//  Created by Etienne Casassa on 3/6/23.
//

/*
 // MARK: Action Plan for Looter:

 
 As of this commit:
 - Working on EquipmentDetailView
 
 - Need to:
    1. Add the ability to randomize more than just 1 item (either set the number in the preferences page and carry it over, or the button just adds a new item)
    2. Add more RELEVANT details to the items in the DetailView and make it look better. Doing this now. Need to add logic to display one type of data vs another depending on the category type. Probably the same I would do with different images.
 
 // TODO: Current issues
 - Need to make it so the handles in the slider cannnot cross each other. Check out the seecond answer here: https://stackoverflow.com/questions/62587261/swiftui-2-handle-range-slider
 - Delete the percentages extra code for the slider. Don't need that.
 
 // MARK: Extra notes:
 - Work on the look of the DetailsView. I'll probably get rid of the image option, since there is no image :(
 - Might not matter yet, but I've been working on the structs to get all the data. But I found that the data can also be looked at from a different angle. https://www.dnd5eapi.co/api/equipment-categories/
 - Idea from Thomas: "Basic idea make one click for setting suggested ranges. For gp you could add some presets such as petty theft. Planned con. Heist. And monster horde.  Each one sets a new range. I am sure there are better descriptions out there than that. Haha. But idea"
 
 // MARK: Adding some early features
 - Bag of gold with a range
 - Allow the user to change how many items I want it to return (and type of items?)
 - Tap on the item and show details (armor, attack, dice used, melee or range, size, cost, etc)
 - A third tab for dice
 - A fourth tab for note keeping for the DM (CoreData)
 - Change the home page (first tab) to a way to navigate through all data ( https://www.dnd5eapi.co/docs/ )
 - Search bar! and a filter option. Probably as a stand-alone tab?
 - I wonder if I could get images for the items? Sadly, the API doesn't provide images :( So maybe have a set of images that show up based on the APIs "EquipmentCategory" property (Tool, Weapon, Armor, etc).
 
 // MARK: Starting out. My MVP
 - Make an API call
 - Display all the items called on a List. This tab will change later. For now it is for proving that the call is being made correctly.
 - Make a second tab where I can tap a button at the top and it shows me... 4 items (or maybe anywhere from 3-5 items)
 
 
 // MARK: Vision/Idea
 - I want to help D&D DMs to create and randomize loot boxes for their campaigns
 
 At first:
 - I want to create an app to be able to randomize through items by type of equipment, strength, range or melee, rarity, etc.
 - Have a bag of gold as well that is randomized, and allows the DM to set a range of gold
 
 Later:
 - Eventually it would be nice to be able to randomize monsters, pick characters, classes, etc. All auto-generated allowing the user to pick some parameters and it spits out results based on preferences
 - Also add campaigns, character names, and you can assign drops from the loot boxes for characters in your compaign
 - Save everything in CoreData at first. Move things to some cloud service later t osave prefereneces based on accounts. Maybe even have people create their own accounts and join campaigns and whatnot?
 */
